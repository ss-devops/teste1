package br.com.southsystem.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.southsystem.service.TransactionService;
import br.com.southsystem.domain.Transaction;

@RestController
@RequestMapping("/transaction")
public class TransactionResource {
	private final TransactionService transactionService;

	public TransactionResource(TransactionService transactionService) {
		this.transactionService = transactionService;
	}


	@PostMapping
	public ResponseEntity<Transaction> add(@RequestBody Transaction transaction) {
		Transaction newTransaction = transactionService.add(transaction);

		return new ResponseEntity<Transaction>(newTransaction, HttpStatus.OK);
	}

}
